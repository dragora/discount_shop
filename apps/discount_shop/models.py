# coding=utf-8
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from apps.categories.models import Category

__author__ = 'dragora'

gender_choices = (('male', 'male'), ('female', 'female'), ('any', 'any'),)


class Brand(models.Model):
    name = models.CharField(u'Бренд товара', max_length=128, unique=True)


class ShopProduct(models.Model):
    name = models.CharField(u'Наименование товара', max_length=128, unique=True)
    category = models.ForeignKey(Category, verbose_name=u'Категория')
    price = models.DecimalField(u'Цена единицы, руб.', max_digits=10, decimal_places=2)
    gender = models.CharField(choices=gender_choices, max_length=16)
    brand = models.ForeignKey(Brand, verbose_name=u'Бренд товара')

    def get_discount_for_product(self):
        try:
            discount = DiscountProduct.objects.get(product=self).discount
        except ObjectDoesNotExist:
            discount = 0

        return discount

    def get_discount_for_brand(self):
        try:
            discount = DiscountBrand.objects.get(brand=self.brand).discount
        except ObjectDoesNotExist:
            discount = 0

        return discount

    def get_discount_for_category(self):
        try:
            discount = DiscountCategory.objects.get(category=self.category).discount
        except ObjectDoesNotExist:
            discount = 0

        return discount

    def get_discount_for_gender(self):
        try:
            discount = DiscountGender.objects.get(gender=self.gender).discount
        except ObjectDoesNotExist:
            discount = 0

        return discount

    def get_discount_for_user(self, user):
        try:
            discount = DiscountUser.objects.get(user=user).discount
        except ObjectDoesNotExist:
            discount = 0

        return discount

    def get_showcase_discount(self):
        discount = max([self.get_discount_for_product(),
                        self.get_discount_for_brand(),
                        self.get_discount_for_category(),
                        self.get_discount_for_gender()])

        return discount

    def discounted_price(self):
        return self.price - (self.price / 100 * self.get_showcase_discount())

    def user_discount(self, user):
        try:
            discount = DiscountUser.objects.get(user=user).discount + self.get_showcase_discount()
        except ObjectDoesNotExist:
            discount = 0

        return discount

    def user_price(self, user):
        return self.price - (self.price / 100 * self.user_discount(user))


class ShopUser(models.Model):
    name = models.CharField(max_length=128, unique=True)
    cart = models.ManyToManyField(ShopProduct, verbose_name=u'Продукты в корзине')


class DiscountBase(models.Model):
    class Meta:
        abstract = True

    discount = models.IntegerField(u'Величина скидки', max_length=2, default=0)

    # Остановка действия скидки
    def stop(self):
        self.discount = 0
        self.save()

    # Установка действия скидки
    def set(self, value):
        self.discount = value
        self.save()


class DiscountProduct(DiscountBase):
    product = models.ForeignKey(ShopProduct, verbose_name=u'Товар по скидке', unique=True)


class DiscountBrand(DiscountBase):
    brand = models.ForeignKey(Brand, verbose_name=u'Бренд по скидке', unique=True)


class DiscountCategory(DiscountBase):
    category = models.ForeignKey(Category, verbose_name=u'Категория по скидке', unique=True)


class DiscountGender(DiscountBase):
    gender = models.CharField(choices=gender_choices, max_length=16, unique=True)


class DiscountUser(DiscountBase):
    user = models.ForeignKey(ShopUser, verbose_name=u'Пользователь со скидкой', unique=True)