# coding=utf-8
__author__ = 'dragora'


def sort_by_showcase_discount_price(objects):
    return sorted(objects, key=lambda product: product.discounted_price())


def sort_by_user_price(objects, user):
    return sorted(objects, key=lambda product: product.user_price(user))