# coding=utf-8
from django.conf.urls import patterns, url

from apps.discount_shop.views import IndexView


__author__ = 'dragora'

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view()),
)