# coding=utf-8
from django.shortcuts import render
from django.views.generic import TemplateView

from apps.discount_shop.models import Category, ShopProduct, Brand, ShopUser, DiscountProduct, DiscountBrand, \
    DiscountCategory, DiscountGender, DiscountUser


__author__ = 'dragora'


class IndexView(TemplateView):
    def get(self, request, *args, **kwargs):

        # Категории
        hi_tech = Category.objects.get_or_create(name='hi-tech')[0]
        furniture = Category.objects.get_or_create(name='furniture')[0]
        clothes = Category.objects.get_or_create(name='clothes')[0]

        # Пользователь
        user = ShopUser.objects.get_or_create(name='Oleg')[0]

        # Бренды
        brand_apple = Brand.objects.get_or_create(name='Apple')[0]
        brand_ms = Brand.objects.get_or_create(name='Microsoft')[0]
        brand_nintendo = Brand.objects.get_or_create(name='Nintendo')[0]
        brand_facebook = Brand.objects.get_or_create(name='Facebook')[0]
        brand_ikea = Brand.objects.get_or_create(name='Ikea')[0]
        brand_infinity = Brand.objects.get_or_create(name='Infinity')[0]
        brand_dng = Brand.objects.get_or_create(name='D%G')[0]

        # Продукты
        laptop = ShopProduct.objects.get_or_create(name='laptop',category=hi_tech, gender='any', brand=brand_apple)[0]
        cellphone = ShopProduct.objects.get_or_create(name='cellphone', category=hi_tech, gender='any', brand=brand_ms)[0]
        console = ShopProduct.objects.get_or_create(name='game console', category=hi_tech, gender='any', brand=brand_nintendo)[0]
        vr_helmet = ShopProduct.objects.get_or_create(name='virtual reality helmet', category=hi_tech, gender='any', brand=brand_facebook)[0]
        sofa = ShopProduct.objects.get_or_create(name='sofa', category=furniture, gender='any', brand=brand_ikea)[0]
        table = ShopProduct.objects.get_or_create(name='table', category=furniture, gender='any', brand=brand_infinity)[0]
        scarf = ShopProduct.objects.get_or_create(name='scarf', category=clothes, gender='male', brand=brand_dng)[0]
        dress = ShopProduct.objects.get_or_create(name='dress', category=clothes, gender='female', brand=brand_dng)[0]

        laptop.price = 300
        laptop.save()
        cellphone.price = 150
        cellphone.save()
        console.price = 150
        console.save()
        vr_helmet.price = 280
        vr_helmet.save()
        sofa.price = 90
        sofa.save()
        table.price = 20
        table.save()
        scarf.price = 5
        scarf.save()
        dress.price = 145
        dress.save()

        # Скидки
        laptop_discount = DiscountProduct.objects.get_or_create(product=laptop)[0]
        apple_discount = DiscountBrand.objects.get_or_create(brand=brand_apple)[0]
        furniture_discount = DiscountCategory.objects.get_or_create(category=furniture)[0]
        user_discount = DiscountUser.objects.get_or_create(user=user)[0]
        female_discount = DiscountGender.objects.get_or_create(gender='female')[0]

        laptop_discount.set(30)
        apple_discount.set(15)
        furniture_discount.set(10)
        user_discount.set(15)
        female_discount.set(14)

        # Текущие скидки
        discounts_product = DiscountProduct.objects.exclude(discount__lte=0)
        discounts_brand = DiscountBrand.objects.exclude(discount__lte=0)
        discounts_category = DiscountCategory.objects.exclude(discount__lte=0)
        discounts_gender = DiscountGender.objects.exclude(discount__lte=0)

        template_name = 'pages/index.html'
        return render(request, template_name,
            {
                # Навигационная информация
                'section': 'main',

                'discounts_product': discounts_product,
                'discounts_brand': discounts_brand,
                'discounts_category': discounts_category,
                'discounts_gender': discounts_gender,
            }
        )