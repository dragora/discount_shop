# coding=utf-8

from django import template
from django.core.exceptions import ObjectDoesNotExist

__author__ = 'dragora'

register = template.Library()


@register.filter(name='user_price')
def user_price(product, user):
    return product.user_price(user)


@register.filter(name='user_discount')
def user_discount(product, user):
    return product.user_discount(user)


@register.filter(name='is_in_cart')
def is_in_cart(product, user):
    cart = user.cart.all()
    try:
        return True if cart.get(name=product.name) else False
    except ObjectDoesNotExist:
        return False