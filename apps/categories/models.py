# coding=utf-8

from django.db import models
from django.core.exceptions import ObjectDoesNotExist

__author__ = 'dragora'


class Category(models.Model):
    name = models.CharField(u'Категория товара', max_length=64, unique=True)