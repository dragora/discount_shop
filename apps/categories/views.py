# coding=utf-8

from django.shortcuts import render
from django.views.generic import TemplateView
from apps.categories.models import Category
from apps.discount_shop.models import ShopProduct, ShopUser
from apps.discount_shop import utils

__author__ = 'dragora'


class CategoriesView(TemplateView):
    def get(self, request, *args, **kwargs):

        categories = Category.objects.all()

        template_name = 'pages/categories.html'
        return render(request, template_name,
            {
                # Навигационная информация
                'section': 'categories',

                'categories': categories,
            }
        )


class CategoryView(TemplateView):
    def get(self, request, *args, **kwargs):

        category_name = kwargs['category_name']

        category = Category.objects.get(name=category_name)
        products = utils.sort_by_showcase_discount_price(ShopProduct.objects.filter(category=category))
        user = ShopUser.objects.get_or_create(name='Oleg')[0]

        template_name = 'pages/category.html'
        return render(request, template_name,
            {
                # Навигационная информация
                'section': 'categories',

                'category': category,
                'products': products,
                'user': user,
            }
        )