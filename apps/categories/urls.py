# coding=utf-8
from django.conf.urls import patterns, url

from views import CategoriesView, CategoryView


__author__ = 'dragora'

urlpatterns = patterns('',
    url(r'^$', CategoriesView.as_view()),
    url(r'^(?P<category_name>[\w-]+)/$', CategoryView.as_view()),
)