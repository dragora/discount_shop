# coding=utf-8

from django.shortcuts import render
from django.views.generic import TemplateView
from apps.discount_shop.models import ShopProduct, ShopUser, DiscountUser

from django.http import HttpResponse, JsonResponse
from django.shortcuts import HttpResponseRedirect

import simplejson as json

__author__ = 'dragora'


class CartView(TemplateView):
    def get(self, request, *args, **kwargs):

        user = ShopUser.objects.get_or_create(name='Oleg')[0]
        user_discount = DiscountUser.objects.get(user=user).discount

        template_name = 'pages/cart.html'
        return render(request, template_name,
            {
                # Навигационная информация
                'section': 'cart',

                'user': user,
                'cart': user.cart.all(),
                'user_discount': user_discount,
            }
        )


class CartAddView(TemplateView):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect('/')

    def post(self, request, *args, **kwargs):
        user = ShopUser.objects.get(name=request.POST['user'])
        product = ShopProduct.objects.get(name=request.POST['product'])
        user.cart.add(product)
        print 'User', user.name, 'added', product.name, 'to the cart!'
        context = {
            'status': 'success'
        }
        return JsonResponse(context)


class CartDelView(TemplateView):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect('/')

    def post(self, request, *args, **kwargs):
        user = ShopUser.objects.get(name=request.POST['user'])
        product = ShopProduct.objects.get(name=request.POST['product'])
        user.cart.remove(product)
        print 'User', user.name, 'removed', product.name, 'from the cart!'
        context = {
            'status': 'success'
        }
        return JsonResponse(context)