# coding=utf-8
from django.conf.urls import patterns, url

from views import CartView, CartAddView, CartDelView


__author__ = 'dragora'

urlpatterns = patterns('',
    url(r'^$', CartView.as_view()),
    url(r'^add/$', CartAddView.as_view()),
    url(r'^del/$', CartDelView.as_view()),
)