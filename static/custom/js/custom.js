/**
 * Created by Олег on 27.06.2015.
 */

$(document).ready(function () {
    $('body').on('click', '.cart-add-toggle', function(e){
        var token = $('input[name="csrfmiddlewaretoken"]').prop('value');
        var btn = $(this);
        var btn_text = btn.text();

        btn.prop('disabled', true);
        btn.text('Обработка...');

        $.ajax({
            type: 'POST',
            url: '/cart/add/',
            data: {'csrfmiddlewaretoken': token,
                'user': user,
                'product': btn.attr('data-product')
            },
            success: function (data) {
                if (data.status == 'success') {
                    btn.prop('disabled', false);
                    btn.text('Убрать из корзины');
                    btn.removeClass('cart-add-toggle');
                    btn.addClass('cart-del-toggle');
                    btn.removeClass('btn-default');
                    btn.addClass('btn-danger');
                }
                else if (data['status'] == 'error'){
                    btn.prop('disabled', false);
                    btn.text(btn_text);
                }
            },
            error: function(data) {
                btn.prop('disabled', false);
                btn.text(btn_text);
            }
        });
    });

    $('body').on('click', '.cart-del-toggle', function(e){
        var token = $('input[name="csrfmiddlewaretoken"]').prop('value');
        var btn = $(this);
        var btn_text = btn.text();

        btn.attr('disabled', 'disabled');
        btn.text('Обработка...');

        $.ajax({
            type: 'POST',
            url: '/cart/del/',
            data: {'csrfmiddlewaretoken': token,
                'user': user,
                'product': btn.attr('data-product')
            },
            success: function (data) {
                if (data.status == 'success') {
                    btn.prop('disabled', false);
                    btn.text('Добавить в корзину');
                    btn.removeClass('cart-del-toggle');
                    btn.addClass('cart-add-toggle');
                    btn.removeClass('btn-danger');
                    btn.addClass('btn-default');
                }
                else if (data['status'] == 'error'){
                    btn.prop('disabled', false);
                    btn.text(btn_text);
                }
            },
            error: function(data) {
                btn.prop('disabled', false);
                btn.text(btn_text);
            }
        });
    });

    $('body').on('click', '.cart-del', function(e){
        var token = $('input[name="csrfmiddlewaretoken"]').prop('value');
        var btn = $(this);
        var btn_text = btn.text();

        btn.attr('disabled', 'disabled');
        btn.text('Обработка...');

        $.ajax({
            type: 'POST',
            url: '/cart/del/',
            data: {'csrfmiddlewaretoken': token,
                'user': user,
                'product': btn.attr('data-product')
            },
            success: function (data) {
                if (data.status == 'success') {
                    location.reload(true);
                }
                else if (data['status'] == 'error'){
                    btn.prop('disabled', false);
                    btn.text(btn_text);
                }
            },
            error: function(data) {
                btn.prop('disabled', false);
                btn.text(btn_text);
            }
        });
    });
});